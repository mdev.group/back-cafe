import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AuthModule } from './modules/auth/auth.module';
import { CafeModule } from './modules/cafe/cafe.module';
import { CompositionModule } from './modules/compostion/composition.module';
import { EmployeeModule } from './modules/employee/employee.module';
import { FileModule } from './modules/files/file.module';
import { ItemModule } from './modules/item/item.module';
import { OrderModule } from './modules/order/order.module';
import { UserModule } from './modules/user/user.module';
import { WorkshiftModule } from './modules/workshift/workshift.module';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: async () => ({
        uri: process.env.DB_CONN,
      }),
    }),
    AuthModule,
    UserModule,
    CafeModule,
    EmployeeModule,
    WorkshiftModule,
    ItemModule,
    CompositionModule,
    FileModule,
    OrderModule
  ]
})
export class AppModule {}
