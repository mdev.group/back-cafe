/* eslint-disable @typescript-eslint/no-var-requires */
import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Request,
  Res,
  Response,
  StreamableFile,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileService } from './file.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { FileInterceptor } from '@nestjs/platform-express';
import { unlinkSync } from 'fs';
import { get as getFile } from 'https';

const imgbbUploader = require("imgbb-uploader");
const detectContentType = require('detect-content-type')

function urlToBuffer(url: string): Promise<Buffer> {
  return new Promise((resolve, reject) => {
    const data: Uint8Array[] = [];
    getFile(url, (res) => {
      res
        .on("data", (chunk: Uint8Array) => {
          data.push(chunk);
        })
        .on("end", () => {
          resolve(Buffer.concat(data));
        })
        .on("error", (err) => {
          reject(err);
        });
    });
  });
}

@Controller()
export class FileController {
  constructor(private readonly fileService: FileService) {}

  @Post('/image')
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(FileInterceptor('file', {dest: '/uploads'}))
  async uploadFile(@UploadedFile() file: Express.Multer.File) {
    const uploadRes = await imgbbUploader("b0ff93be482efd77da21551da4fc083e", file.path)
      .then((response) => response)
      .catch((error) => error);

    unlinkSync(file.path);

    return {
      size: uploadRes.size,
      url: uploadRes.image.url.replace('https://i.ibb.co/', 'https://api.m-dev.group/image/')
    };
  }

  @Get('/image/:id/:name')
  async getFile(@Param() params, @Response({ passthrough: true }) res): Promise<StreamableFile> {
    const url = `https://i.ibb.co/${params.id}/${params.name}`;
    
    const buffer = await urlToBuffer(url);

    res.set({
      'Content-Type': detectContentType(buffer),
    });

    return new StreamableFile(buffer);
  }
}
