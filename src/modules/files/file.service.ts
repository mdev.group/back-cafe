import { Model, FilterQuery } from 'mongoose';
import { Injectable, ForbiddenException, NotAcceptableException, NotFoundException, Inject, forwardRef } from '@nestjs/common';
import { Item, ItemDocument } from 'src/schemas/item.schema';
import { InjectModel } from '@nestjs/mongoose';
import { removeEmptyFields } from 'src/utils/removeEmptyFields';
import { generate } from 'generate-password';
import { removeFieldsIf } from 'src/utils/removeFieldsIf';
import { Cafe, CafeDocument } from 'src/schemas/cafe.schema';
import { CafeService } from '../cafe/cafe.service';
import { letFields } from 'src/utils/letFields';
import { CompositionService } from '../compostion/composition.service';

@Injectable()
export class FileService {
  async appendComposition(): Promise<any> {
    return true
  }
}
