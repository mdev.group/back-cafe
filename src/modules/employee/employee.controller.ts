import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { Employee } from 'src/schemas/employee.schema';
import { EmployeeService } from './employee.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { CreateEmployeeDto } from './dto/create-employee.dto';

@Controller()
export class EmployeeController {
  constructor(private readonly employeeService: EmployeeService) {}

  @UseGuards(JwtAuthGuard)
  @Get('/employee/:id')
  async getByID(@Request() req, @Param() params): Promise<Employee> {
    const res = await this.employeeService.findByID(params.id, req.user);

    if(!res) {
      throw new NotFoundException();
    }

    return res;
  }

  @UseGuards(JwtAuthGuard)
  @Get('/employee/cafe/:id')
  async getByCafe(@Request() req, @Param() params): Promise<Employee[]> {
    const res = await this.employeeService.findByCafe(params.id, req.user);

    if(!res) {
      throw new NotFoundException();
    }

    return res;
  }

  @UseGuards(JwtAuthGuard)
  @Patch('/employee/:id')
  update(@Request() req, @Body() data: UpdateEmployeeDto, @Param() params) {
    return this.employeeService.update(params.id, data, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Patch('/employee/:id/fire')
  fire(@Request() req, @Body() data: any, @Param() params) {
    return this.employeeService.fire(params.id, data.value, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/employee')
  create(@Request() req, @Body() data: CreateEmployeeDto) {
    return this.employeeService.create(data, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('/employee/:id')
  delete(@Request() req, @Param() params) {
    return this.employeeService.delete(params.id, req.user);
  }
}
