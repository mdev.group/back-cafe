import { Model, FilterQuery } from 'mongoose';
import { Injectable, ForbiddenException, NotAcceptableException, NotFoundException, Inject, forwardRef } from '@nestjs/common';
import { Employee, EmployeeDocument } from 'src/schemas/employee.schema';
import { InjectModel } from '@nestjs/mongoose';
import { removeEmptyFields } from 'src/utils/removeEmptyFields';
import { generate } from 'generate-password';
import { removeFieldsIf } from 'src/utils/removeFieldsIf';
import { Cafe, CafeDocument } from 'src/schemas/cafe.schema';
import { CafeService } from '../cafe/cafe.service';

@Injectable()
export class EmployeeService {
  constructor(
    @InjectModel(Employee.name) private EmployeeModel: Model<EmployeeDocument>,
    @Inject(forwardRef(() => CafeService))
    private cafeService: CafeService,
  ) {}

  async findAll(user): Promise<Employee[]> {
    const res = await this.EmployeeModel.find().exec();
    return res;
  }

  async findByID(ID, user): Promise<Employee> {
    const res = await this.EmployeeModel.findById(ID).lean().exec();
    if(!res) {
      throw new NotFoundException('Employee');
    }

    const isCafeOwner = await this.cafeService.isCafeOwner(user, res.cafeID);

    return removeFieldsIf(res, ['tgID', 'secret', 'hireDate', 'fireDate'], !isCafeOwner);
  }

  async findByCafe(cafeID, user): Promise<EmployeeDocument[]> {
    try {
      const isCafeOwner = user === 'system' || await this.cafeService.isCafeOwner(user, cafeID) || await this.cafeService.isCafeEmployee(user, cafeID);
      
      const filter: FilterQuery<EmployeeDocument> = {
        cafeID
      };

      const res = await this.EmployeeModel.find(filter).exec();
      return res.map((item) => removeFieldsIf(item.toObject(), ['tgID', 'secret', 'hireDate', 'fireDate'], !isCafeOwner));
    } catch {
      return null;
    }
  }


  async fire(ID, value, user): Promise<Employee> {
    let item: EmployeeDocument;
    try {
      item = await this.EmployeeModel.findById(ID);
    } catch {
      throw new NotFoundException('employee');
    }

    if(!item) {
      throw new NotFoundException('employee');
    }

    const isCafeOwner = await this.cafeService.isCafeOwner(user, item.cafeID);
    if(!isCafeOwner) {
      throw new NotAcceptableException();
    }

    await item.updateOne({
      $set: {
        fireDate: value? Math.floor(Number(new Date()) / 1000) : null,
      }, 
    }, {
      new: true
    }).exec();

    return {
      ...item.toObject(),
      fireDate: value? Math.floor(Number(new Date()) / 1000) : null,
    };
  }
  

  async update(ID, data, user): Promise<Employee> {
    let item: EmployeeDocument;
    try {
      item = await this.EmployeeModel.findById(ID);
    } catch {
      throw new NotFoundException();
    }

    if(!item) {
      throw new NotFoundException();
    }

    const isCafeOwner = await this.cafeService.isCafeOwner(user, item.cafeID);
    if(!isCafeOwner) {
      throw new NotAcceptableException();
    }

    await item.updateOne({
      $set: {
        ...removeEmptyFields(data),
      }, 
    }, {
      new: true
    }).exec();

    return {...item.toObject(), ...removeEmptyFields(data)};
  }

  async getExistEmployeeCodesByCafeID(cafeID, user): Promise<string[]> {
    const employees = await this.findByCafe(cafeID, user);
    return employees.map((item) => item.secret);
  }
  
  async create(data, user): Promise<Employee> {
    const isCafeOwner = await this.cafeService.isCafeOwner(user, data.cafeID);
    if(!isCafeOwner) {
      throw new NotAcceptableException("Cafe");
    }

    const excludeCodes = await this.getExistEmployeeCodesByCafeID(data.cafeID, user);
    let secret;

    do {
      secret = generate({
        length: 4,
        numbers: true,
        symbols: false,
        lowercase: false,
        uppercase: false
      });
    } while (excludeCodes.includes(secret))

    const created = new this.EmployeeModel({
      cafeID: data.cafeID,
      first_name: data.first_name,
      last_name: data.last_name,
      position: data.position,
      hireDate: Math.floor(Number(new Date()) / 1000),
      secret
    });

    if(created) {
      return created.save();
    } else {
      throw new ForbiddenException('save error');
    }
  }
  
  async delete(employeeID, user): Promise<any> {
    let item: EmployeeDocument;
    try {
      item = await this.EmployeeModel.findById(employeeID);
    } catch {
      throw new NotFoundException("Employee");
    }
    if(!item) {
      throw new NotFoundException("Employee");
    }

    const isCafeOwner = await this.cafeService.isCafeOwner(user, item.cafeID);
    if(!isCafeOwner) {
      throw new NotAcceptableException("Cafe");
    }

    try {
      await item.deleteOne();
      return {
        res: true
      };
    } catch (err) {
      console.log(err);
      throw new ForbiddenException('Delete error');
    }
  }
}
