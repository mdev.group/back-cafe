import { ApiProperty } from '@nestjs/swagger';

export class CreateEmployeeDto {
  @ApiProperty()
  cafeID: string;

  @ApiProperty()
  first_name: string;

  @ApiProperty()
  last_name: string;
}
