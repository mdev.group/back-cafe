import { Model, FilterQuery } from 'mongoose';
import { Injectable, ForbiddenException, NotAcceptableException, NotFoundException, forwardRef, Inject } from '@nestjs/common';
import { Cafe, CafeDocument, ELoyaltyType } from 'src/schemas/cafe.schema';
import { InjectModel } from '@nestjs/mongoose';
import { CreateCafeDto } from './dto/create-cafe.dto';
import { removeEmptyFields } from 'src/utils/removeEmptyFields';
import { generate } from 'generate-password';
import { removeFieldsIf } from 'src/utils/removeFieldsIf';
import { letFields } from 'src/utils/letFields';
import { EmployeeService } from 'src/modules/employee/employee.service';
import { WorkshiftService } from '../workshift/workshift.service';
import { ItemService } from '../item/item.service';

@Injectable()
export class CafeService {
  constructor(
    @InjectModel(Cafe.name) private CafeModel: Model<CafeDocument>,
    @Inject(forwardRef(() => EmployeeService))
    private employeeService: EmployeeService,
    @Inject(forwardRef(() => WorkshiftService))
    private workshiftService: WorkshiftService,
    @Inject(forwardRef(() => ItemService))
    private itemService: ItemService,
  ) {}

  async isCafeOwner(user, cafeID) {
    const cafe: CafeDocument = await this.CafeModel.findById(cafeID);

    if(!cafe) {
      throw new NotFoundException();
    }

    return cafe.ownerID == user.userId;
  }

  async isCafeEmployee(user, cafeID) {
      const employees = await this.employeeService.findByCafe(cafeID, 'system')
      return employees.some((item) => item._id == user.userId);
  }

  async findAll(filter: any): Promise<Cafe[]> {
    const res = await this.CafeModel.find({
      ...filter.ids ? {
        _id: {
          $in: filter.ids
        },
        accepted: true
      } : {
        accepted: true
      }
    }).exec();
    return Promise.all(res.map(async (item) => removeFieldsIf((await this.appendItems(item.toObject(), 'system')), ['secret'], true)));
  }

  async appendEmployes(cafe: any, user): Promise<any> {
    cafe.employees = await this.employeeService.findByCafe(cafe._id, user);
    return cafe;
  }

  async appendWorkshifts(cafe: any, user): Promise<any> {
    cafe.workshifts = await this.workshiftService.findByCafeID(cafe._id, user);
    return cafe;
  }

  async appendItems(cafe: any, user): Promise<any> {
    cafe.items = await this.itemService.findByCafe(cafe._id, user);
    return cafe;
  }

  async appendEntries(cafe: CafeDocument, user): Promise<any> {
    let res = cafe.toObject();
    
    res = await this.appendEmployes(res, user);
    res = await this.appendWorkshifts(res, user);
    res = await this.appendItems(res, user);

    return res;
  }

  async findByID(ID, user): Promise<Cafe> {
    try {
      const filter: FilterQuery<CafeDocument> = {
        _id: ID
      };

      const item = await this.CafeModel.findOne(filter).exec();

      return removeFieldsIf(await this.appendEntries(item, user), ['secret'], item.ownerID !== user.userId);
    } catch {
      return null;
    }
  }

  async findByOwner(ownerID, user): Promise<Cafe[]> {
    try {
      const filter: FilterQuery<CafeDocument> = {
        ownerID: ownerID
      };

      const res = await this.CafeModel.find(filter).exec();
      return Promise.all(res.map(async (item) => removeFieldsIf((await this.appendEntries(item, user)), ['secret'], item.ownerID !== user.userId)));
    } catch {
      return null;
    }
  }

  async findByCode(code, user): Promise<CafeDocument> {
    try {
      const filter: FilterQuery<CafeDocument> = {
        code
      };

      const res = await this.CafeModel.findOne(filter).exec();
      return removeFieldsIf((await this.appendEntries(res, user)), ['secret'], res.ownerID !== user.userId)
    } catch {
      return null;
    }
  }

  async findByEmployee(user): Promise<Cafe[]> {
    try {
      const employee = await this.employeeService.findByID(user.userId, user)

      const item = await this.CafeModel.findById(employee.cafeID).exec();

      return removeFieldsIf(await this.appendEntries(item, user), ['secret'], item.ownerID !== user.userId);
    } catch {
      return null;
    }
  }

  async update(ID, data, user): Promise<Cafe> {
    let item;
    try {
      item = await this.CafeModel.findById(ID);
    } catch {
      throw new NotFoundException();
    }

    if(!item) {
      throw new NotFoundException();
    }

    if(item.ownerID !== user.userId) {
      throw new NotAcceptableException();
    }

    const updates = letFields(removeEmptyFields(data), ['location', 'acceptOnlineOrders', 'badges', 'categories', 'name', 'photo_url', 'address', 'closed', 'loyaltyType']);

    await item.updateOne({
      $set: {
        ...updates,
      }, 
    }, {
      new: true
    }).exec();

    return {...item.toObject(), ...updates};
  }
  
  async create(data, user): Promise<Cafe> {
    try {
      const secret = generate({
        length: 16,
        numbers: true
      });

      const excludeCodes = (await this.CafeModel.find().exec()).map((client) => client.code)
    
      let code;
      do {
        code = generate({
          length: 6,
          numbers: true,
          symbols: false,
          lowercase: false,
          uppercase: true
        });
      } while (excludeCodes.includes(code))

      const created = new this.CafeModel({
        ownerID: user.userId,
        creationDate: Math.floor(Number(new Date()) / 1000),
        address: data.address,
        name: data.name,
        loyaltyType: ELoyaltyType.DISABLED,
        closed: true,
        secret,
        code,
        items: []
      });

      if(created) {
        return created.save();
      } else {
        throw new ForbiddenException();
      }
    } catch {
      throw new ForbiddenException();
    }
  }
}
