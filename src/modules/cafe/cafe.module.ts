import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Cafe, CafeSchema } from 'src/schemas/cafe.schema';
import { CafeController } from './cafe.controller';
import { CafeService } from './cafe.service';
import { EmployeeModule } from 'src/modules/employee/employee.module';
import { WorkshiftModule } from '../workshift/workshift.module';
import { ItemModule } from '../item/item.module';

@Module({
  controllers: [CafeController],
  providers: [CafeService],
  imports: [
    MongooseModule.forFeature([{ name: Cafe.name, schema: CafeSchema }]),
    forwardRef(() => EmployeeModule),
    forwardRef(() => WorkshiftModule),
    forwardRef(() => ItemModule)
  ],
  exports: [
    MongooseModule.forFeature([{ name: Cafe.name, schema: CafeSchema }]),
    CafeService,
  ],
})
export class CafeModule {}
