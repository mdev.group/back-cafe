import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { Cafe } from 'src/schemas/cafe.schema';
import { CafeService } from './cafe.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { UpdateCafeDto } from './dto/update-cafe.dto';
import RoleGuard from '../auth/role.guard';
import { Role } from '../auth/role.enum';

@Controller()
export class CafeController {
  constructor(private readonly cafeService: CafeService) {}

  @Post('/cafe/get')
  async getAllCafes(@Request() req, @Body() data): Promise<Cafe[]> {
    return await this.cafeService.findAll(data);
  }

  @UseGuards(RoleGuard([Role.Employee]))
  @Get('/cafe/my')
  async getMyCafe(@Request() req): Promise<Cafe[]> {
    return await this.cafeService.findByEmployee(req.user);
  }

  @UseGuards(RoleGuard([Role.Owner]))
  @Get('/cafe/owned')
  async getAllOwnedCafes(@Request() req): Promise<Cafe[]> {
    const res = await this.cafeService.findByOwner(req.user.userId, req.user);

    if(!res) {
      return [];
    }

    return res;
  }

  @UseGuards(JwtAuthGuard)
  @Get('/cafe/:id')
  async getCafe(@Request() req, @Param() params): Promise<Cafe> {
    const res = await this.cafeService.findByID(params.id, req.user);

    if(!res) {
      throw new NotFoundException();
    }

    return res;
  }

  @UseGuards(JwtAuthGuard)
  @Patch('/cafe/:id')
  create(@Request() req, @Body() data: UpdateCafeDto, @Param() params) {
    return this.cafeService.update(params.id, data, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/cafe')
  update(@Request() req, @Body() data: UpdateCafeDto) {
    return this.cafeService.create(data, req.user);
  }
}
