import { ApiProperty } from '@nestjs/swagger';

export class CreateCafeDto {
  @ApiProperty()
  name?: string;

  @ApiProperty()
  address?: string;
}
