import { ApiProperty } from '@nestjs/swagger';

export class UpdateCafeDto {
  @ApiProperty()
  name?: string;

  @ApiProperty()
  address?: string;
  
  @ApiProperty()
  photo_url?: string;
}
