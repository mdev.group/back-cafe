import { Controller, Request, Post, UseGuards, Get, Body, SetMetadata } from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './jwt-auth.guard';
import { CustomAuthGuard } from './custom-auth.guard';
import { EmployeeAuthGuard } from './employee-auth.guard';
import { ClientAuthGuard } from './client-auth.guard';

@Controller()
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(CustomAuthGuard)
  @Post('auth/login')
  @SetMetadata('allow-any', true)
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @UseGuards(ClientAuthGuard)
  @Post('auth/login/client')
  @SetMetadata('allow-any', true)
  async loginClient(@Request() req) {
    return this.authService.login(req.user);
  }

  @UseGuards(EmployeeAuthGuard)
  @Post('auth/login/employee')
  @SetMetadata('allow-any', true)
  async loginEmployee(@Request() req) {
    return this.authService.login(req.user);
  }

  @Post('auth/refresh')
  async refresh(@Body() data) {
    return this.authService.refresh(data.token);
  }

  @UseGuards(JwtAuthGuard)
  @Get('auth')
  getProfile(@Request() req) {
    return req.user;
  }
}
