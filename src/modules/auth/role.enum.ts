export enum Role {
  Client = 'client',
  Owner = 'owner',
  Employee = 'employee',
}