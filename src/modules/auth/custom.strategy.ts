import { Strategy } from 'passport-custom';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { Request } from 'express';
import { TelegramLogin, TelegramLoginPayload } from 'node-telegram-login';
import { Role } from './role.enum';

@Injectable()
export class CustomStrategy extends PassportStrategy(Strategy) {
  static key = "custom"
  siteLogin = new TelegramLogin(process.env.TG_TOKEN);

  constructor(private authService: AuthService) {
    super();
  }

  async validate(req: Request): Promise<any> {
    // Максимальное время отклонения
    const MAX_TIME_DELTA = 10;
    const {userType, ...data} = req.body as (TelegramLoginPayload & {userType: Role});

    // Проверка валидности хеша
    if (!this.siteLogin.checkLoginData(data)) {
      throw new UnauthorizedException('Invalid data');
    }

    if(process.env.NODE_ENV != 'development') {
      // Проверка просрочки авторизации. Она не должна быть старше MAX_TIME_DELTA
      if (Math.abs(+data.auth_date - Number(new Date()) / 1000) > MAX_TIME_DELTA) {
        throw new UnauthorizedException('Expired');
      }
    }

    // Создаем нового пользователя или возвращаем существующего
    const user = await this.authService.readOrCreate(data, userType);
    if (!user) {
      throw new UnauthorizedException();
    }
    
    return user;
  }
}
