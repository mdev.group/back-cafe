import { Strategy } from 'passport-custom';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { Request } from 'express';
import { Role } from './role.enum';

@Injectable()
export class EmployeeStrategy extends PassportStrategy(Strategy, 'employee') {
  static key = "employee"

  constructor(private authService: AuthService) {
    super();
  }

  async validate(req: Request): Promise<any> {
    const {userType, ...data} = req.body as ({code: string, cafeCode: string} & {userType: Role});

    // Создаем нового пользователя или возвращаем существующего
    const user = await this.authService.readOrCreate(data, userType);
    if (!user) {
      throw new UnauthorizedException();
    }
    
    return user;
  }
}
