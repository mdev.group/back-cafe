import { CanActivate, ExecutionContext, mixin, Type } from '@nestjs/common';
import { JwtAuthGuard } from './jwt-auth.guard';
import { Role } from './role.enum';
 
const RoleGuard = (roles: Role[]): Type<CanActivate> => {
  class RoleGuardMixin extends JwtAuthGuard {
    async canActivate(context: ExecutionContext) {
      await super.canActivate(context);
 
      const request = context.switchToHttp().getRequest();
      const user = request.user;

      return (user?.roles as Role[]).some((item) => roles.includes(item));
    }
  }
 
  return mixin(RoleGuardMixin);
}
 
export default RoleGuard;