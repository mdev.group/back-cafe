import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserModule } from '../user/user.module';
import { PassportModule } from '@nestjs/passport';
import { CustomStrategy } from './custom.strategy';
import { AuthController } from './auth.controller';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './jwt.strategy';
import { EmployeeModule } from '../employee/employee.module';
import { EmployeeStrategy } from './employee.strategy';
import { ClientModule } from '../client/client.module';
import { ClientStrategy } from './client.strategy';
import { CafeModule } from '../cafe/cafe.module';

@Module({
  controllers: [AuthController],
  imports: [
    UserModule,
    PassportModule,
    EmployeeModule,
    ClientModule,
    CafeModule,
    JwtModule.registerAsync({
      useFactory: () => {
        return {
          secret: process.env.JWT_SECRET,
          signOptions: { expiresIn: '24h' },
        };
      },
    }),
  ],
  providers: [AuthService, CustomStrategy, JwtStrategy, EmployeeStrategy, ClientStrategy],
})
export class AuthModule {}
