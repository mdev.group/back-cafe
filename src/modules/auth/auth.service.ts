import { Injectable, NotAcceptableException, NotFoundException } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { compareSync } from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { TelegramLoginPayload } from 'node-telegram-login';
import { verify } from 'jsonwebtoken';
import { Role } from './role.enum';
import { EmployeeService } from '../employee/employee.service';
import { ClientService } from '../client/client.service';
import { CafeService } from '../cafe/cafe.service';

interface IEmployeeLoginPayload {
  code: string;
  cafeCode: string;
}

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private clientService: ClientService,
    private employeeService: EmployeeService,
    private cafeService: CafeService,
    private jwtService: JwtService,
  ) {}

  async readOrCreate(data: TelegramLoginPayload | IEmployeeLoginPayload, userType: Role): Promise<any> {
    switch (userType) {
      case Role.Owner:
        return (async () => {
          const logindata = data as TelegramLoginPayload;
          let user = await this.userService.findByTgID(logindata.id);
          if(!user) {
            await this.userService.create({
              auth_date: logindata.auth_date,
              first_name: logindata.first_name,
              last_name: logindata.last_name,
              photo_url: logindata.photo_url,
              id: logindata.id
            });
  
            user = await this.userService.findByTgID(logindata.id);
          }
  
          const { ...userData } = user;
          return {
            ...userData,
            userType
          };
        })()
      case Role.Client:
        return (async () => {
          const logindata = data as TelegramLoginPayload;
          let user = await this.clientService.findByTgID(logindata.id);
          if(!user) {
            await this.clientService.create({
              auth_date: logindata.auth_date,
              first_name: logindata.first_name,
              last_name: logindata.last_name,
              photo_url: logindata.photo_url,
              id: logindata.id
            });
  
            user = await this.clientService.findByTgID(logindata.id);
          }
  
          const { ...userData } = user;
          return {
            ...userData,
            userType
          };
        })()
      case Role.Employee:
        return (async () => {
          const logindata = data as IEmployeeLoginPayload;

          const cafe = await this.cafeService.findByCode(logindata.cafeCode, 'system');
          if(!cafe) {
            throw new NotFoundException('Cafe')
          }
  
          const employees = await this.employeeService.findByCafe(cafe._id, 'system');
          const employee = employees.find((employee) => employee.secret === logindata.code)
          if(!employee) {
            throw new NotFoundException('Employee')
          }
  
          const { secret, ...userData } = employee;
          return {
            ...userData,
            userType
          };
        })()
    }
  }

  async login(user: any) {
    const roles = [user.userType]

    const payload =
    {
      sub: user._id,
      iat: Math.floor(Number(new Date()) / 1000),
      tgID: user.tgID,
      roles
    };
    return {
      access_token: this.jwtService.sign(payload, {
        expiresIn: '120s'
      }),
      refresh_token: this.jwtService.sign(payload, {
        expiresIn: '30d'
      }),
      expires_in: 120
    };
  }

  async refresh(oldToken: any) {
    try {
      const old_payload = verify(oldToken, process.env.JWT_SECRET) as any;

      const payload =
      {
        sub: old_payload.sub,
        iat: Math.floor(Number(new Date()) / 1000),
        tgID: old_payload.tgID,
        roles: old_payload.roles
      };
      return {
        access_token: this.jwtService.sign(payload, {
          expiresIn: '120s'
        }),
        refresh_token: this.jwtService.sign(payload, {
          expiresIn: '30d'
        }),
        expires_in: 120
      };
    } catch {
      return null
    }
  }
}
