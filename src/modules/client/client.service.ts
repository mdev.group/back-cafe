import { Model, FilterQuery } from 'mongoose';
import { Injectable, ForbiddenException } from '@nestjs/common';
import { Client, ClientDocument } from 'src/schemas/client.schema';
import { InjectModel } from '@nestjs/mongoose';
import { removeEmptyFields } from 'src/utils/removeEmptyFields';
import { generate } from 'generate-password';
import { letFields } from 'src/utils/letFields';

@Injectable()
export class ClientService {
  constructor(
    @InjectModel(Client.name) private ClientModel: Model<ClientDocument>,
  ) {}

  async create(data): Promise<Client> {
    try {
      
    const excludeCodes = (await this.ClientModel.find().exec()).map((client) => client.code)
    
    let code;
    do {
      code = generate({
        length: 6,
        numbers: true,
        symbols: false,
        lowercase: false,
        uppercase: true
      });
    } while (excludeCodes.includes(code))

      const created = new this.ClientModel({
        tgID: data.id,
        reg_datetime: Number(data.auth_date),
        first_name: data.first_name,
        last_name: data.last_name,
        photo_url: data.photo_url,
        code 
      });

      if(created) {
        return await created.save();
      } else {
        throw new ForbiddenException();
      }
    } catch {
      throw new ForbiddenException();
    }
  }

  async findAll(): Promise<Client[]> {
    return this.ClientModel.find().exec();
  }

  async findByID(ID): Promise<Client> {
    try {
      const filter: FilterQuery<ClientDocument> = {
        _id: ID
      };

      return await this.ClientModel.findOne(filter).lean().exec();
    } catch {
      return null;
    }
  }

  async findByTgID(tgID): Promise<Client> {
    try {
      const filter: FilterQuery<ClientDocument> = {
        tgID,
      };
  
      return await this.ClientModel.findOne(filter).lean().exec();
    } catch {
      return null;
    }
  }

  async findByCode(code): Promise<Client> {
    try {
      const filter: FilterQuery<ClientDocument> = {
        code,
      };
  
      return await this.ClientModel.findOne(filter).lean().exec();
    } catch {
      return null;
    }
  }

  async update(ID, data): Promise<Client> {
    return this.ClientModel
      .findByIdAndUpdate(ID, {
        $set: {
          ...letFields(removeEmptyFields(data), ['info', 'first_name', 'last_name', 'photo_url', 'birth_date']),
        },
      },
      {new: true})
      .exec();
  }

  async scoresEdit(ID: string, diffValue: number): Promise<Client> {
    return this.ClientModel
      .findByIdAndUpdate(ID, {
        $inc: {
          scores: diffValue
        },
      },
      {new: true})
      .exec();
  }
}
