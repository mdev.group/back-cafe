import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Client, ClientSchema } from 'src/schemas/client.schema';
import { EmployeeModule } from '../employee/employee.module';
import { ClientController } from './client.controller';
import { ClientService } from './client.service';

@Module({
  controllers: [ClientController],
  providers: [ClientService],
  imports: [
    MongooseModule.forFeature([{ name: Client.name, schema: ClientSchema }]),
  ],
  exports: [
    MongooseModule.forFeature([{ name: Client.name, schema: ClientSchema }]),
    ClientService,
  ],
})
export class ClientModule {}
