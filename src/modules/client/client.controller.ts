import {
  Body,
  Controller,
  forwardRef,
  Get,
  Inject,
  NotFoundException,
  Param,
  Patch,
  Request,
  UseGuards,
} from '@nestjs/common';
import { Client } from 'src/schemas/client.schema';
import { ClientService } from './client.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { EmployeeService } from '../employee/employee.service';
import { Employee } from 'src/schemas/employee.schema';
import RoleGuard from '../auth/role.guard';
import { Role } from '../auth/role.enum';

@Controller()
export class ClientController {
  constructor(
    private readonly clientService: ClientService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get('/client/:id')
  async getClient(@Param() params): Promise<Client> {
    return await this.clientService.findByID(params.id);
  }

  @UseGuards(JwtAuthGuard)
  @Patch('/client')
  update(@Request() req, @Body() updateClientDto) {
    return this.clientService.update(req.user.userId, updateClientDto);
  }

  @UseGuards(RoleGuard([Role.Employee, Role.Owner]))
  @Get('/client/code/:code')
  findByPhone(@Request() req, @Param() params) {
    return this.clientService.findByCode(params.code);
  }
}
