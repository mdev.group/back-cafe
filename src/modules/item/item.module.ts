import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Item, ItemSchema } from 'src/schemas/item.schema';
import { CafeModule } from '../cafe/cafe.module';
import { CompositionModule } from '../compostion/composition.module';
import { ItemController } from './item.controller';
import { ItemService } from './item.service';

@Module({
  controllers: [ItemController],
  providers: [ItemService],
  imports: [
    MongooseModule.forFeature([{ name: Item.name, schema: ItemSchema }]),
    forwardRef(() => CafeModule),
    forwardRef(() => CompositionModule)
  ],
  exports: [
    MongooseModule.forFeature([{ name: Item.name, schema: ItemSchema }]),
    ItemService,
  ],
})
export class ItemModule {}
