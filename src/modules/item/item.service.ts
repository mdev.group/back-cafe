import { Model, FilterQuery } from 'mongoose';
import { Injectable, ForbiddenException, NotAcceptableException, NotFoundException, Inject, forwardRef } from '@nestjs/common';
import { Item, ItemDocument } from 'src/schemas/item.schema';
import { InjectModel } from '@nestjs/mongoose';
import { removeEmptyFields } from 'src/utils/removeEmptyFields';
import { generate } from 'generate-password';
import { removeFieldsIf } from 'src/utils/removeFieldsIf';
import { Cafe, CafeDocument } from 'src/schemas/cafe.schema';
import { CafeService } from '../cafe/cafe.service';
import { letFields } from 'src/utils/letFields';
import { CompositionService } from '../compostion/composition.service';

@Injectable()
export class ItemService {
  constructor(
    @InjectModel(Item.name) private ItemModel: Model<ItemDocument>,
    @Inject(forwardRef(() => CafeService))
    private cafeService: CafeService,
    @Inject(forwardRef(() => CompositionService))
    private compositionService: CompositionService,
  ) {}

  async appendComposition(item: any, user): Promise<any> {
    const item_compositions = await this.compositionService.findByIDs(item.composition.map((item) => item.id), user);

    item.compositionItems = item_compositions;

    return item;
  }

  async appendEntries(cafe: ItemDocument, user): Promise<any> {
    let res = cafe.toObject();
    
    res = await this.appendComposition(res, user);

    return res;
  }

  async findByID(ID, user): Promise<Item> {
    const filter: FilterQuery<ItemDocument> = {
      _id: ID
    };

    const res = await this.ItemModel.findOne(filter).exec();
    if(!res) {
      throw new NotFoundException('Item');
    }
    
    return this.appendEntries(res, user);
  }

  async findByCafe(cafeID, user): Promise<Item[]> {
    const filter: FilterQuery<ItemDocument> = {
      cafeID
    };

    const res = await this.ItemModel.find(filter).exec();
    if(!res) {
      throw new NotFoundException('Item');
    }

    return await Promise.all(res.map((item) => this.appendEntries(item, user)));
  }

  async update(ID, data, user): Promise<Item> {
    let item: ItemDocument;
    try {
      item = await this.ItemModel.findById(ID);
    } catch {
      throw new NotFoundException('Item');
    }

    if(!item) {
      throw new NotFoundException('Item');
    }

    const isCafeOwner = await this.cafeService.isCafeOwner(user, item.cafeID);
    if(!isCafeOwner) {
      throw new NotAcceptableException('Item - Cafe');
    }

    const newData = letFields(removeEmptyFields(data), ['category', 'name', 'mass', 'photo_url', 'composition', 'price']);

    await item.updateOne({
      $set: {
        ...newData,
      }, 
    }, {
      new: true
    }).exec();

    return {...item.toObject(), ...newData};
  }
  
  async create(data, user): Promise<Item> {
    const isCafeOwner = await this.cafeService.isCafeOwner(user, data.cafeID);
    if(!isCafeOwner) {
      throw new NotAcceptableException("Item - Cafe");
    }

    const created = new this.ItemModel({
      cafeID: data.cafeID,
      name: data.name,
      mass: data.mass,
      price: data.price,
      composition: []
    });

    if(created) {
      return created.save();
    } else {
      throw new ForbiddenException('save error');
    }
  }
  
  async delete(itemID, user): Promise<any> {
    let item: ItemDocument;
    try {
      item = await this.ItemModel.findById(itemID);
    } catch {
      throw new NotFoundException("Item");
    }
    if(!item) {
      throw new NotFoundException("Item");
    }

    const isCafeOwner = await this.cafeService.isCafeOwner(user, item.cafeID);
    if(!isCafeOwner) {
      throw new NotAcceptableException("Item - Cafe");
    }

    try {
      await item.deleteOne();
      return {
        res: true
      };
    } catch (err) {
      console.log(err);
      throw new ForbiddenException('Delete error');
    }
  }
}
