import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { Item } from 'src/schemas/item.schema';
import { ItemService } from './item.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

@Controller()
export class ItemController {
  constructor(private readonly itemService: ItemService) {}

  @UseGuards(JwtAuthGuard)
  @Get('/item/:id')
  async getByID(@Request() req, @Param() params): Promise<Item> {
    return await this.itemService.findByID(params.id, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/item/cafe/:id')
  async getByCafe(@Request() req, @Param() params): Promise<Item[]> {
    return await this.itemService.findByCafe(params.id, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Patch('/item/:id')
  update(@Request() req, @Body() data, @Param() params) {
    return this.itemService.update(params.id, data, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/item')
  create(@Request() req, @Body() data) {
    return this.itemService.create(data, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('/item/:id')
  delete(@Request() req, @Param() params) {
    return this.itemService.delete(params.id, req.user);
  }
}
