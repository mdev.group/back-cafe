import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { Composition } from 'src/schemas/composition.schema';
import { CompositionService } from './composition.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

@Controller()
export class CompositionController {
  constructor(private readonly compositionService: CompositionService) {}

  @UseGuards(JwtAuthGuard)
  @Post('/composition')
  create(@Request() req, @Body() data) {
    return this.compositionService.create(data, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/composition')
  readAll(@Request() req) {
    return this.compositionService.findAll(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/composition/search')
  findByName(@Request() req, @Body() data) {
    return this.compositionService.searchByName(data.searchText, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Patch('/composition/:id')
  update(@Request() req, @Body() data, @Param() params) {
    return this.compositionService.update(params.id, data, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('/composition/:id')
  delete(@Request() req, @Param() params) {
    return this.compositionService.delete(params.id, req.user);
  }
}
