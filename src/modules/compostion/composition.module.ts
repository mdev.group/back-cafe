import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Composition, CompositionSchema } from 'src/schemas/composition.schema';
import { CafeModule } from '../cafe/cafe.module';
import { CompositionController } from './composition.controller';
import { CompositionService } from './composition.service';

@Module({
  controllers: [CompositionController],
  providers: [CompositionService],
  imports: [
    MongooseModule.forFeature([{ name: Composition.name, schema: CompositionSchema }]),
  ],
  exports: [
    MongooseModule.forFeature([{ name: Composition.name, schema: CompositionSchema }]),
    CompositionService,
  ],
})
export class CompositionModule {}
