import { Model, FilterQuery } from 'mongoose';
import { Injectable, ForbiddenException, NotAcceptableException, NotFoundException, Inject, forwardRef } from '@nestjs/common';
import { Composition, CompositionDocument } from 'src/schemas/composition.schema';
import { InjectModel } from '@nestjs/mongoose';
import { removeEmptyFields } from 'src/utils/removeEmptyFields';
import { generate } from 'generate-password';
import { removeFieldsIf } from 'src/utils/removeFieldsIf';
import { Cafe, CafeDocument } from 'src/schemas/cafe.schema';
import { CafeService } from '../cafe/cafe.service';
import { letFields } from 'src/utils/letFields';

@Injectable()
export class CompositionService {
  constructor(
    @InjectModel(Composition.name) private CompositionModel: Model<CompositionDocument>,
  ) {}

  async findAll(user): Promise<Composition[]> {
    const res = await this.CompositionModel.find().lean().exec();

    if(!res) {
      throw new NotFoundException('Composition');
    }
    
    return res;
  }

  async findByIDs(IDs: string[], user): Promise<Composition[]> {
    const res = await this.CompositionModel.find({
      _id: {
        $in: IDs
      }
    }).exec();

    if(!res) {
      throw new NotFoundException('Composition');
    }
    
    return res.map((item) => (item.toObject()));
  }


  async findByCreator(creatorID, user): Promise<Composition[]> {
    if(creatorID !== user.userId) {
      throw new NotAcceptableException('Composition')
    }

    const filter: FilterQuery<CompositionDocument> = {
      creatorID
    };

    const res = await this.CompositionModel.find(filter).exec();
    if(!res) {
      throw new NotFoundException('Composition');
    }

    return res.map((composition) => composition.toObject());
  }

  async update(ID, data, user): Promise<Composition> {
    let composition: CompositionDocument;
    try {
      composition = await this.CompositionModel.findById(ID);
    } catch {
      throw new NotFoundException("Composition");
    }
    if(!composition) {
      throw new NotFoundException("Composition");
    }

    const isUserOwner = composition.creatorID === user.userId;
    if(!isUserOwner) {
      throw new NotAcceptableException("Composition");
    }

    const newData = letFields(removeEmptyFields(data), ['pfc', 'name']);

    await composition.updateOne({
      $set: {
        ...newData,
      }, 
    }, {
      new: true
    }).exec();

    return {...composition.toObject(), ...newData};
  }
  
  async create(data, user): Promise<Composition> {
    const created = new this.CompositionModel({
      creatorID: user.userId,
      pfc: data.pfc,
      name: data.name
    });

    if(created) {
      return created.save();
    } else {
      throw new ForbiddenException('save error');
    }
  }
  
  async delete(compositionID, user): Promise<any> {
    let composition: CompositionDocument;
    try {
      composition = await this.CompositionModel.findById(compositionID);
    } catch {
      throw new NotFoundException("Composition");
    }
    if(!composition) {
      throw new NotFoundException("Composition");
    }

    const isUserOwner = composition.creatorID === user.userId;
    if(!isUserOwner) {
      throw new NotAcceptableException("Composition");
    }

    try {
      await composition.deleteOne();
      return {
        res: true
      };
    } catch (err) {
      console.log(err);
      throw new ForbiddenException('Delete error');
    }
  }

  async searchByName(search, user): Promise<Composition[]> {
    return await this.CompositionModel.aggregate([
    {
      $match: {
        name: {$regex: search, $options: 'i'}
      }
    },
    {
      $limit: 4
    }
    ])
    .exec()
  }
}
