import {
  Body,
  Controller,
  forwardRef,
  Get,
  Inject,
  NotFoundException,
  Param,
  Patch,
  Request,
  UseGuards,
} from '@nestjs/common';
import { User } from 'src/schemas/user.schema';
import { UserService } from './user.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { UpdateUserDto } from './dto/update-user.dto';
import { EmployeeService } from '../employee/employee.service';
import { Employee } from 'src/schemas/employee.schema';
import { Role } from '../auth/role.enum';
import { ClientService } from '../client/client.service';

@Controller()
export class UserController {
  constructor(
    private readonly userService: UserService,
    @Inject(forwardRef(() => EmployeeService))
    private employeeService: EmployeeService,
    @Inject(forwardRef(() => ClientService))
    private clientService: ClientService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get('/user')
  async getAllUsers(): Promise<User[]> {
    return await this.userService.findAll();
  }

  @UseGuards(JwtAuthGuard)
  @Get('/user/:id')
  async getSelf(@Request() req, @Param() params): Promise<User | Employee> {
    let user: User | Employee;

    if(params.id === 'self') {
      if(req.user.roles.includes(Role.Owner)) {
        user = await this.userService.findByTgID(req.user.tgID);
      }
      if(req.user.roles.includes(Role.Employee)) {
        user = await this.employeeService.findByID(req.user.userId, req.user);
      }
      if(req.user.roles.includes(Role.Client)) {
        user = await this.clientService.findByID(req.user.userId);
      }
    } else {
      user = await this.userService.findByID(params.id);
    }

    if(!user) {
      throw new NotFoundException();
    }

    return user;
  }

  @UseGuards(JwtAuthGuard)
  @Patch('/user')
  update(@Request() req, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.update(req.user.userId, updateUserDto);
  }
}
