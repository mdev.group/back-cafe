import { Model, FilterQuery } from 'mongoose';
import { Injectable, ForbiddenException } from '@nestjs/common';
import { User, UserDocument } from 'src/schemas/user.schema';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { removeEmptyFields } from 'src/utils/removeEmptyFields';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private UserModel: Model<UserDocument>,
  ) {}

  async create(data: CreateUserDto): Promise<User> {
    try {
      const created = new this.UserModel({
        tgID: data.id,
        reg_datetime: Number(data.auth_date),
        first_name: data.first_name,
        last_name: data.last_name,
        photo_url: data.photo_url
      });

      if(created) {
        return await created.save();
      } else {
        throw new ForbiddenException();
      }
    } catch {
      throw new ForbiddenException();
    }
  }

  async findAll(): Promise<User[]> {
    return this.UserModel.find().exec();
  }

  async findByID(ID): Promise<User> {
    try {
      const filter: FilterQuery<UserDocument> = {
        _id: ID
      };

      return await this.UserModel.findOne(filter).lean().exec();
    } catch {
      return null;
    }
  }

  async findByTgID(tgID): Promise<User> {
    try {
      const filter: FilterQuery<UserDocument> = {
        tgID,
      };
  
      return await this.UserModel.findOne(filter).lean().exec();
    } catch {
      return null;
    }
  }

  async update(ID, data): Promise<User> {
    return this.UserModel
      .findByIdAndUpdate(ID, {
        $set: {
          ...removeEmptyFields(data),
        },
      },
      {new: true})
      .exec();
  }
}
