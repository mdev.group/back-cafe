import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Order, OrderSchema } from 'src/schemas/order.schema';
import { CafeModule } from '../cafe/cafe.module';
import { ClientModule } from '../client/client.module';
import { OrderController } from './order.controller';
import { OrderService } from './order.service';

@Module({
  controllers: [OrderController],
  providers: [OrderService],
  imports: [
    MongooseModule.forFeature([{ name: Order.name, schema: OrderSchema }]),
    forwardRef(() => CafeModule),
    forwardRef(() => ClientModule)
  ],
  exports: [
    MongooseModule.forFeature([{ name: Order.name, schema: OrderSchema }]),
    OrderService,
  ],
})
export class OrderModule {}
