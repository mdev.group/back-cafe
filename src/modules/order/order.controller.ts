import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { Order } from 'src/schemas/order.schema';
import { OrderService } from './order.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

@Controller()
export class OrderController {
  constructor(private readonly orderService: OrderService) {}

  @UseGuards(JwtAuthGuard)
  @Get('/order/cafe/:id')
  async getOrdersByCafe(@Request() req, @Param() params): Promise<Order[]> {
    return await this.orderService.findByCafeID(params.id, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/order/client')
  async getOrdersByClient(@Request() req, @Param() params): Promise<Order[]> {
    return await this.orderService.findByClientID(req.user.userId, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/order/:id')
  async getOrderByID(@Request() req, @Param() params): Promise<Order> {
    return await this.orderService.findByID(params.id, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Patch('/order/:id')
  async update(@Request() req, @Body() data, @Param() params) {
    return await this.orderService.update(params.id, data, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('/order/:id')
  async delete(@Request() req, @Body() data, @Param() params) {
    return await this.orderService.delete(params.id, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/order')
  async create(@Request() req, @Body() data) {
    return await this.orderService.create(data, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/order/payment')
  async payment(@Request() req, @Body() data) {
    return await this.orderService.payment(data.orderID, req.user);
  }
}
