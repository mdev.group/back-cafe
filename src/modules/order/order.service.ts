import { Model, FilterQuery } from 'mongoose';
import { Injectable, ForbiddenException, NotAcceptableException, NotFoundException, forwardRef, Inject } from '@nestjs/common';
import { EOrderStatus, Order, OrderDocument } from 'src/schemas/order.schema';
import { InjectModel } from '@nestjs/mongoose';
import { removeEmptyFields } from 'src/utils/removeEmptyFields';
import { generate } from 'generate-password';
import { removeFieldsIf } from 'src/utils/removeFieldsIf';
import { CafeService } from '../cafe/cafe.service';
import { v4 as uuidv4 } from 'uuid';
import { ClientService } from '../client/client.service';

@Injectable()
export class OrderService {
  constructor(
    @InjectModel(Order.name) private OrderModel: Model<OrderDocument>,
    @Inject(forwardRef(() => CafeService))
    private cafeService: CafeService,
    @Inject(forwardRef(() => ClientService))
    private clientService: ClientService,
  ) {}

  async findByID(ID, user): Promise<Order> {
    const res = await this.OrderModel.findById(ID).lean().exec();

    return res;
  }

  async findByCafeID(cafeID, user): Promise<Order[]> {
    const res = await this.OrderModel.find({cafeID}).lean().exec();

    return res;
  }
  
  async findActiveByCafeID(cafeID, user): Promise<Order[]> {
    const res = await this.OrderModel.find({
      cafeID,
      status: {
        $ne: EOrderStatus.done
      }
    }).lean().exec();

    return res;
  }

  async findByClientID(clientID, user): Promise<Order[]> {
    const res = await this.OrderModel.find({clientID}).lean().exec();

    return res;
  }
  
  async findByEmployeeID(employeeID, user): Promise<Order[]> {
    const res = await this.OrderModel.find({employeeID}).lean().exec();

    return res;
  }

  async update(ID, data, user): Promise<Order> {
    let item: OrderDocument;
    
    try {
      item = await this.OrderModel.findById(ID);
    } catch {
      throw new NotFoundException('Order');
    }

    if(!item) {
      throw new NotFoundException('Order');
    }

    const isCafeEmployee = await this.cafeService.isCafeEmployee(user, item.cafeID);
    if (!isCafeEmployee) {
      throw new NotAcceptableException("!Employee -> Cafe -> Order");
    }

    if(data.status === EOrderStatus.approved) {
      data.pickedTime = Number(new Date());
    }


    if(data.status === EOrderStatus.done) {
      throw new NotAcceptableException("Status === done -> update() -> Order | Use /order/payment");
    }

    if(data.positions) {
      data.positions = data.positions.map((item) => ({
        ...item,
        uuid: (item.uuid && item.uuid != '') ? item.uuid : uuidv4()
      }))
    }

    await item.updateOne({
      $set: {
        ...removeEmptyFields(data)
      }, 
    }, {
      new: true
    }).exec();

    return {...item.toObject(), ...removeEmptyFields(data)};
  }

  async delete(ID, user): Promise<boolean> {
    let item: OrderDocument;
    
    try {
      item = await this.OrderModel.findById(ID);
    } catch {
      throw new NotFoundException('Order');
    }
    if(!item) {
      throw new NotFoundException('Order');
    }

    const isCafeEmployee = await this.cafeService.isCafeEmployee(user, item.cafeID);
    if (!isCafeEmployee) {
      throw new NotAcceptableException("!Employee -> Cafe -> Order");
    }

    await item.delete();

    return true;
  }
  
  async create(data, user): Promise<Order> {
    const isCafeEmployee = await this.cafeService.isCafeEmployee(user, data.cafeID);

    const ordersActive = await this.findActiveByCafeID(data.cafeID, user);
    const excludeCodes = ordersActive.map((item) => item.number);

    let code;
    do {
      code = generate({
        length: 4,
        numbers: true,
        symbols: false,
        lowercase: false,
        uppercase: false
      });
    } while (excludeCodes.includes(code))


    const created = new this.OrderModel({
      cafeID: data.cafeID,
      beginTime: Number(new Date()),
      positions: data.positions.map((item) => ({
        ...item,
        uuid: uuidv4()
      })),
      number: code,

      ...isCafeEmployee ? {
        employeeID: user.userId,
        table: data.table,
        status: EOrderStatus.form,
      } : {
        clientID: user.userId,
        takeTime: data.takeTime,
        status: EOrderStatus.created,
      },
    });

    if(created) {
      return created.save();
    } else {
      throw new ForbiddenException('Save -> Order');
    }
  }

  async payment(ID, user): Promise<Order> {
    let item: OrderDocument;
    
    try {
      item = await this.OrderModel.findById(ID);
    } catch {
      throw new NotFoundException('Order');
    }

    if(!item) {
      throw new NotFoundException('Order');
    }

    const isCafeEmployee = await this.cafeService.isCafeEmployee(user, item.cafeID);
    if (!isCafeEmployee) {
      throw new NotAcceptableException("!Employee -> Cafe -> Order");
    }

    if(item.status !== EOrderStatus.paytime) {
      throw new NotAcceptableException("Status != Paytime -> Cafe -> Order");
    }

    if(item.payByScores > 0 && item.clientID) {
      await this.clientService.scoresEdit(item.clientID, -item.payByScores)
    }

    await item.updateOne({
      $set: {
        status: EOrderStatus.done,
        endTime: Number(new Date())
      }, 
    }, {
      new: true
    }).exec();

    return {
      ...item.toObject(),
      status: EOrderStatus.done
    };
  }
}