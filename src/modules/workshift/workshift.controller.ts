import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { Workshift } from 'src/schemas/workshift.schema';
import { WorkshiftService } from './workshift.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { UpdateWorkshiftDto } from './dto/update-workshift.dto';
import { CreateWorkshiftDto } from './dto/create-workshift.dto';

@Controller()
export class WorkshiftController {
  constructor(private readonly workshiftService: WorkshiftService) {}

  @UseGuards(JwtAuthGuard)
  @Get('/workshift/cafe/:id')
  async getWorkshiftsByCafe(@Request() req, @Param() params): Promise<Workshift[]> {
    const res = await this.workshiftService.findByCafeID(params.id, req.user);

    if(!res) {
      throw new NotFoundException();
    }

    return res;
  }

  @UseGuards(JwtAuthGuard)
  @Get('/workshift/:id')
  async getWorkshiftByID(@Request() req, @Param() params): Promise<Workshift> {
    const res = await this.workshiftService.findByID(params.id, req.user);

    if(!res) {
      throw new NotFoundException();
    }

    return res;
  }

  @UseGuards(JwtAuthGuard)
  @Get('/workshift/employee/:id')
  async getWorkshiftByEmployeeID(@Request() req, @Param() params): Promise<Workshift[]> {
    const res = await this.workshiftService.findByEmployeeID(params.id, req.user);

    if(!res) {
      throw new NotFoundException();
    }

    return res;
  }

  @UseGuards(JwtAuthGuard)
  @Patch('/workshift/:id')
  update(@Request() req, @Body() data: UpdateWorkshiftDto, @Param() params) {
    return this.workshiftService.update(params.id, data, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/workshift')
  create(@Request() req, @Body() data: CreateWorkshiftDto) {
    return this.workshiftService.create(data, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/workshift/:id/employee')
  assignEmployee(@Request() req, @Body() data: any, @Param() params) {
    return this.workshiftService.assignEmployee(params.id, data.employeeID, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('/workshift/:id/employee')
  unassignEmployee(@Request() req, @Body() data: any, @Param() params) {
    return this.workshiftService.unassignEmployee(params.id, data.employeeID, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('/workshift/:id')
  deleteWorkshift(@Request() req, @Param() params) {
    return this.workshiftService.delete(params.id, req.user);
  }
}
