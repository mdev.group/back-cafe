import { Model, FilterQuery } from 'mongoose';
import { Injectable, ForbiddenException, NotAcceptableException, NotFoundException, forwardRef, Inject } from '@nestjs/common';
import { Workshift, WorkshiftDocument } from 'src/schemas/workshift.schema';
import { InjectModel } from '@nestjs/mongoose';
import { CreateWorkshiftDto } from './dto/create-workshift.dto';
import { removeEmptyFields } from 'src/utils/removeEmptyFields';
import { generate } from 'generate-password';
import { removeFieldsIf } from 'src/utils/removeFieldsIf';
import { CafeService } from '../cafe/cafe.service';

@Injectable()
export class WorkshiftService {
  constructor(
    @InjectModel(Workshift.name) private WorkshiftModel: Model<WorkshiftDocument>,
    @Inject(forwardRef(() => CafeService))
    private cafeService: CafeService,
  ) {}

  async findByID(ID, user): Promise<Workshift> {
    try {
      const filter: FilterQuery<WorkshiftDocument> = {
        _id: ID
      };

      const res = await this.WorkshiftModel.findOne(filter).lean().exec();

      return res;
    } catch {
      return null;
    }
  }

  async findByCafeID(ID, user): Promise<Workshift[]> {
    try {
      const filter: FilterQuery<WorkshiftDocument> = {
        cafeID: ID
      };

      const res = await this.WorkshiftModel.find(filter).lean().exec();

      return res;
    } catch {
      return null;
    }
  }

  
  async findByEmployeeID(ID, user): Promise<Workshift[]> {
    try {
      const filter: FilterQuery<WorkshiftDocument> = {
        employeeIDs: {
          $in: [ID]
        }
      };

      const res = await this.WorkshiftModel.find(filter).lean().exec();

      return res;
    } catch {
      return null;
    }
  }

  async update(ID, data, user): Promise<Workshift> {
    let item: WorkshiftDocument;
    try {
      item = await this.WorkshiftModel.findById(ID);
    } catch {
      throw new NotFoundException();
    }

    if(!item) {
      throw new NotFoundException();
    }

    const isCafeOwner = await this.cafeService.isCafeOwner(user, item.cafeID);
    if (!isCafeOwner) {
      throw new NotAcceptableException("Cafe");
    }

    await item.updateOne({
      $set: {
        ...removeEmptyFields(data),
      }, 
    }, {
      new: true
    }).exec();

    return {...item.toObject(), ...removeEmptyFields(data)};
  }
  
  async create(data: CreateWorkshiftDto, user): Promise<Workshift> {
    try {
      const isCafeOwner = await this.cafeService.isCafeOwner(user, data.cafeID);
      if (!isCafeOwner) {
        throw new NotAcceptableException("Cafe");
      }

      const created = new this.WorkshiftModel({
        cafeID: data.cafeID,
        dayOfWeek: data.dayOfWeek,
        beginTime: data.beginTime,
        endTime: data.endTime,
        employeeIDs: []
      });

      if(created) {
        return created.save();
      } else {
        throw new ForbiddenException();
      }
    } catch {
      throw new ForbiddenException();
    }
  }

  async assignEmployee(workshiftID, employeeID, user): Promise<Workshift> {
    let item: WorkshiftDocument;
    try {
      item = await this.WorkshiftModel.findById(workshiftID);
    } catch {
      throw new NotFoundException();
    }

    if(!item) {
      throw new NotFoundException();
    }

    const isCafeOwner = await this.cafeService.isCafeOwner(user, item.cafeID);
    if (!isCafeOwner) {
      throw new NotAcceptableException("Cafe");
    }

    await item.updateOne({
      $addToSet: { employeeIDs: employeeID }
    }, {
      new: true
    }).exec();

    const resSet = new Set<string>([...item.employeeIDs, employeeID]);

    return {
      ...item.toObject(),
      employeeIDs: [...resSet]
    };
  }

  async unassignEmployee(workshiftID, employeeID, user): Promise<Workshift> {
    let item: WorkshiftDocument;
    try {
      item = await this.WorkshiftModel.findById(workshiftID);
    } catch {
      throw new NotFoundException();
    }

    if(!item) {
      throw new NotFoundException();
    }

    const isCafeOwner = await this.cafeService.isCafeOwner(user, item.cafeID);
    if (!isCafeOwner) {
      throw new NotAcceptableException("Cafe");
    }

    await item.updateOne({
      $pull: { employeeIDs: employeeID }
    }, {
      new: true
    }).exec();

    return {
      ...item.toObject(),
      employeeIDs: item.employeeIDs.filter((item) => item != employeeID)
    };
  }

  async delete(workshiftID, user): Promise<boolean> {
    let item: WorkshiftDocument;
    try {
      item = await this.WorkshiftModel.findById(workshiftID);
    } catch {
      throw new NotFoundException();
    }

    if(!item) {
      throw new NotFoundException();
    }

    const isCafeOwner = await this.cafeService.isCafeOwner(user, item.cafeID);
    if (!isCafeOwner) {
      throw new NotAcceptableException("Cafe");
    }

    await item.delete();

    return true;
  }
}