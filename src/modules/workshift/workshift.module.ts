import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Workshift, WorkshiftSchema } from 'src/schemas/workshift.schema';
import { CafeModule } from '../cafe/cafe.module';
import { WorkshiftController } from './workshift.controller';
import { WorkshiftService } from './workshift.service';

@Module({
  controllers: [WorkshiftController],
  providers: [WorkshiftService],
  imports: [
    MongooseModule.forFeature([{ name: Workshift.name, schema: WorkshiftSchema }]),
    forwardRef(() => CafeModule)
  ],
  exports: [
    MongooseModule.forFeature([{ name: Workshift.name, schema: WorkshiftSchema }]),
    WorkshiftService,
  ],
})
export class WorkshiftModule {}
