import { ApiProperty } from '@nestjs/swagger';
import { EDayOfWeek } from 'src/schemas/workshift.schema';

export class UpdateWorkshiftDto {
  @ApiProperty()
  beginTime: string;

  @ApiProperty()
  endTime: string;

  @ApiProperty(
    {
      minimum: 1,
      maximum: 7,
      required: true
    }
  )
  dayOfWeek: EDayOfWeek;
}
