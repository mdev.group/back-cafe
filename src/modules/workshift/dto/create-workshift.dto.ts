import { ApiProperty } from '@nestjs/swagger';
import { EDayOfWeek } from 'src/schemas/workshift.schema';

export class CreateWorkshiftDto {
  @ApiProperty()
  cafeID: string;

  @ApiProperty()
  beginTime: number;

  @ApiProperty()
  endTime: number;

  @ApiProperty(
    {
      minimum: 1,
      maximum: 7,
      required: true
    }
  )
  dayOfWeek: EDayOfWeek;
}
