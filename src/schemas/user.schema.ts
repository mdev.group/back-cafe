import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class User {
  @Prop({ required: true, unique: true })
  tgID: number;

  @Prop()
  first_name: string;

  @Prop()
  last_name: string;

  @Prop()
  photo_url: string;

  @Prop({ required: true })
  reg_datetime: number;
}

export type UserDocument = User & Document;
export const UserSchema = SchemaFactory.createForClass(User);
