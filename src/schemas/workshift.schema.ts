import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export enum EDayOfWeek {
  monday = 1,
  thuesday = 2,
  wednesday = 3,
  thursday = 4,
  friday = 5,
  saturday = 6,
  sunday = 7
}

@Schema()
export class Workshift {
  @Prop({ required: true })
  employeeIDs: string[];

  @Prop({ required: true })
  cafeID: string;

  @Prop({ required: true, max: 1440, min: 0 })
  beginTime: number;

  @Prop({ required: true, max: 1440, min: 0 })
  endTime: number;

  @Prop({ required: true })
  dayOfWeek: number;
}

export type WorkshiftDocument = Workshift & Document;
export const WorkshiftSchema = SchemaFactory.createForClass(Workshift);
