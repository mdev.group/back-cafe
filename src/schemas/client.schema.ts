import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Client {
  @Prop({ required: true, unique: true })
  tgID: number;

  @Prop({ required: true, unique: true })
  code: string;

  @Prop()
  first_name: string;

  @Prop()
  last_name: string;

  @Prop()
  photo_url: string;

  @Prop()
  info: string;

  @Prop({ required: true })
  reg_datetime: number;

  @Prop()
  birth_date: number;

  @Prop({ default: 0, required: true })
  scores: number;
}

export type ClientDocument = Client & Document;
export const ClientSchema = SchemaFactory.createForClass(Client);
