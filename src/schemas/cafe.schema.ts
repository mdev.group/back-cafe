import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export enum ELoyaltyType {
  DISABLED,
  LOCAL,
  GLOBAL,
}

export class Location {
  lat: string;
  lng: string;
}

@Schema()
export class Cafe {
  @Prop({ required: true })
  ownerID: string;

  @Prop({ required: true })
  code: string;

  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  secret: string;

  @Prop({ required: true })
  loyaltyType: ELoyaltyType;

  @Prop()
  photo_url?: string;

  @Prop()
  address?: string;

  @Prop()
  location?: Location;

  @Prop({ required: true })
  creationDate: number;

  @Prop({ required: true })
  closed: boolean;

  @Prop({ required: true, default: [] })
  categories: string[];

  @Prop({ required: true, default: [] })
  badges: string[];

  @Prop({ required: true, default: false })
  acceptOnlineOrders: boolean;

  @Prop({ required: true, default: false })
  accepted: boolean;
}

export type CafeDocument = Cafe & Document;
export const CafeSchema = SchemaFactory.createForClass(Cafe);
