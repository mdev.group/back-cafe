import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export class IPFC {
  @Prop({ required: true })
  proteins: number; //x4

  @Prop({ required: true })
  fats: number; //x9

  @Prop({ required: true })
  carbohydrates: number; //x4
}

@Schema()
export class Composition {
  @Prop({ required: true })
  creatorID: string;

  @Prop({ required: true })
  name: string;

  @Prop()
  pfc?: IPFC;
}

export type CompositionDocument = Composition & Document;
export const CompositionSchema = SchemaFactory.createForClass(Composition);
