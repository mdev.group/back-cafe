import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export enum EOrderStatus {
  form,
  created,
  approved,
  serve,
  checktime,
  loyalty,
  scoresChoose,
  paySelection,
  paytime,
  done
}

export class IOrderItemIngredients {
  @Prop({ required: true })
  id: string;

  @Prop({ required: true })
  count: number;

  @Prop({ required: true })
  defaultCount: number;

  @Prop({ required: true })
  price: number;

  @Prop({ required: true })
  isEditable: boolean;
}


export class OrderPosition {
  @Prop({ required: true })
  itemID: string;

  @Prop({ required: true })
  orderTime: number;

  @Prop({ required: true, default: []})
  ingredients: IOrderItemIngredients[]

  @Prop({ required: true })
  uuid: string;

  @Prop({ required: true })
  fixed: boolean;
}

@Schema()
export class Order {
  @Prop({ required: true })
  number: string;

  @Prop({ required: true })
  cafeID: string;

  @Prop({ required: true })
  positions: OrderPosition[]

  // Время создания заказа
  @Prop({ required: true })
  beginTime: number;

  // Врямя принятия заказа
  @Prop()
  pickedTime: number;

  // Время закрытия заказа
  @Prop()
  endTime: number;

  // Время для готовности заказа
  @Prop()
  takeTime: number;

  @Prop()
  employeeID: string;

  @Prop()
  table: string | null;

  @Prop()
  payByCard: number;

  @Prop()
  payByPaper: number;
  
  @Prop()
  payByScores: number;

  @Prop()
  status: EOrderStatus;

  @Prop()
  clientID: string;

  @Prop()
  precheck: boolean;
}

export type OrderDocument = Order & Document;
export const OrderSchema = SchemaFactory.createForClass(Order);
