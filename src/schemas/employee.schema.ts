import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Employee {
  @Prop({ required: true })
  cafeID: string;

  @Prop()
  tgID: number;

  @Prop({ required: true })
  first_name: string;

  @Prop({ required: true })
  last_name: string;

  @Prop({ required: true })
  secret: string;

  @Prop({ required: true })
  hireDate: number;

  @Prop()
  photo_url?: string;

  @Prop()
  fireDate: number;

  @Prop({ required: true })
  position: string;
}

export type EmployeeDocument = Employee & Document;
export const EmployeeSchema = SchemaFactory.createForClass(Employee);
