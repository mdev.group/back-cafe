import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

interface ICompositionAssign {
  id: string;
  mass: number;
  isEditable: boolean;
  price: number;
}

@Schema()
export class Item {
  @Prop({ required: true })
  cafeID: string;

  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  mass: number;

  @Prop()
  photo_url?: string;

  @Prop({ default: [] })
  composition: ICompositionAssign[];

  @Prop({ default: 0, required: true })
  price: number;

  @Prop()
  category: string;
}

export type ItemDocument = Item & Document;
export const ItemSchema = SchemaFactory.createForClass(Item);
