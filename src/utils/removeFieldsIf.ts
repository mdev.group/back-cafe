export function removeFieldsIf<T>(obj: T, removeKeys: (keyof T)[], condition: boolean): any {
  return Object.fromEntries(
    Object.keys(obj)
      .filter((key) => !condition || !removeKeys.includes(key as any))
      .map((key) => {
        return [key, obj[key]];
      }),
  );
}
